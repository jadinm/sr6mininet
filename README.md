# SR6Mininet #

This is a python library, extending [IPMininet](https://github.com/oliviertilmans/ipmininet), in order
to support emulation of [IPv6 Segment Routing](http://segment-routing.org) networks.
As such it extends classes of IPMininet to enable IPv6 Segment Routing and insertion of rules on the routers.

[sr6mininet/examples/README](https://bitbucket.org/jadinm/sr6mininet/sr6mininet/examples/README.md) contains
a description of the examples that come with this library.

### Setup ###

- A linux kernel with IPv6 Segment Routing and the tool iproute2 are needed. Instructions are detailed in
[SRv6 web site](http://segment-routing.org/index.php/Implementation/Installation).

- [IPMininet](https://github.com/oliviertilmans/ipmininet)
