"""This module has a hard dependency against mininet, check here that it is
actually installed. This will yield a better (?) error message than just a raw
ImportError nested somewhere ..."""

try:
	import ipmininet  # noqa
except ImportError as e:
	import sys
	sys.stderr.write('Failed to import ipmininet!\n'
	                 'Please install it.\n')
	sys.exit(1)
