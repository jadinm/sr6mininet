"""An enhanced CLI providing SR6-related commands"""
from mininet.log import lg
from mininet.node import Switch

from ipmininet.cli import IPCLI
from ipmininet.utils import realIntfList


def address_pair(n, use_v4=True, use_v6=True):
    """Returns a tuple (ip, ip6) with ip/ip6 being one of the IPv4/IPv6
       addresses of the node n"""
    v4 = v6 = None
    for itf in realIntfList(n):
        if use_v4 and v4 is None:
            v4 = itf.updateIP()
        if use_v6 and v6 is None:
            itf.updateIP6()
            v6 = next(itf.ip6s(exclude_lls=True, exclude_lbs=True), None)
            v6 = v6.ip.compressed if v6 is not None else v6
        if (not use_v4 or v4 is not None) and (not use_v6 or v6 is not None):
            break

    # Replace by node's SID
    if 'lo' in n.intfNames():
        for ip6 in n.intf('lo').ip6s(exclude_lls=True, exclude_lbs=True):
            v6 = ip6.ip.compressed
            break
    return v4, v6


class SR6CLI(IPCLI):
    def default(self, line):
        """Called on an input line when the command prefix is not recognized.
        Overridden to run shell commands when a node is the first CLI argument.
        Past the first CLI argument, node names are automatically replaced with
        corresponding addresses if possible.
        We select only one IP version for these automatic replacements.
        The chosen IP version chosen is first restricted by the addresses
        available on the first node.
        Then, we choose the IP version that enables every replacement.
        We use IPv4 as a tie-break."""

        first, args, line = self.parseline(line)

        if first in self.mn:
            if not args:
                print("*** Enter a command for node: %s <cmd>" % first)
                return
            node = self.mn[first]
            rest = args.split(' ')

            hops = [h for h in rest if h in self.mn and not isinstance(self.mn[h], Switch)]
            v4_support, v6_support = address_pair(self.mn[first])
            v4_map = {}
            v6_map = {}
            for hop in hops:
                ip, ip6 = address_pair(self.mn[hop],
                                       v4_support is not None,
                                       v6_support is not None)
                if ip is not None:
                    v4_map[hop] = ip
                if ip6 is not None:
                    v6_map[hop] = ip6
            # IPv6 is the default
            ip_map = v4_map if len(v4_map) > len(v6_map) else v6_map

            if len(ip_map) < len(hops):
                missing = filter(lambda h: h not in ip_map, hops)
                version = 'IPv4' if v4_support else 'IPv6'
                lg.error('*** Nodes', missing, 'have no', version,
                         'address! Cannot execute the command.\n')
                return

            node.sendCmd(' '.join([ip_map.get(r, r) for r in rest]))
            self.waitForNode(node)
        else:
            lg.error('*** Unknown command: %s\n' % line)
