# Example topologies

This directory contains example topologies, you can start them using
```bash
python -m sr6mininet.examples --topo=[topo_name] [--args key=val,key=val]
```
Where topo_name is the name of the topology, and args are optional arguments
for it.

The following sections will detail the topologies.

   - [SimpleSRNetwork](#simplesrnetwork)
   - [SimpleStaticSRNetwork](#simplestaticsrnetwork)

## SimpleSRNetwork

_topo name_ : simple_sr_network
_args_ : n/a

This network spawn a single AS topology, using OSPFv3,
with variable link metrics and two IPv6 Segment Routing routes.
From the mininet CLI, access the routers vtysh using
```bash
[noecho rx] telnet localhost [ospf6d/zebra]
```
Where the noecho rx is required if you don't use a separate xterm window for
the node (via `xterm rx`), and ospf6d/zebra is the name of the daemon you wish
to connect to.

## SimpleStaticSRNetwork

_topo name_ : simple_static_sr_network
_args_ : n/a

This network spawn a single AS topology, with variable link metrics
and two IPv6 Segment Routing routes.
Static routes are computed for each node so that any node is reachable from any other node,
However the routes for **routers** are computed only for the loopback addresses,
not the interfaces address.
Thus, you have to force the source IP of packets to be the loopback address.

For example, if you wish to ping router *r7* from *r4*, use
```bash
$ ping6 -I lo_r4 lo_r7
```
Note that in Mininet CLI (i.e., not in a separate xterm window), router names
are automatically replaced by their loopback addresses.
Therefore, if you wish to ping router *r7* from *r4*, you can use
```bash
mininet> r4 ping6 -I r4 r7
```
