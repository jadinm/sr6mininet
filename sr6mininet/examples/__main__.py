"This files lets you start all examples"
import argparse
from mininet.log import lg, LEVELS

import ipmininet

from sr6mininet.cli import SR6CLI
from sr6mininet.sr6net import SR6Net
from .delay_sr_network import DelaySRNet
from .ecn_sr_network import ECNSRNet
from .lossy_sr_network import LossySRNet
from .simple_sr_network import SimpleSRNet

TOPOS = {
    'simple_sr_network': SimpleSRNet,
    'simple_static_sr_network': SimpleSRNet,
    'lossy_network': LossySRNet,
    'static_lossy_network': LossySRNet,
    'ecn_network': ECNSRNet,
    'static_ecn_network': ECNSRNet,
    'delay_network': DelaySRNet,
    'static_delay_network': DelaySRNet
}

NET_ARGS = {
    'simple_static_sr_network': {'static_routing': True},
    'lossy_network': {'allocate_IPs': False},
    'static_lossy_network': {'static_routing': True, 'allocate_IPs': False},
    'ecn_network': {'allocate_IPs': False},
    'static_ecn_network': {'static_routing': True, 'allocate_IPs': False},
    'delay_network': {'allocate_IPs': False},
    'static_delay_network': {'static_routing': True, 'allocate_IPs': False}
}


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--topo', choices=TOPOS.keys(),
                        default='simple_sr_network',
                        help='The topology that you want to start.')
    parser.add_argument('--log', choices=LEVELS.keys(), default='info',
                        help='The level of details in the logs.')
    parser.add_argument('--args', help='Additional arguments to give'
                                       'to the topology constructor (key=val, key=val, ...)',
                        default='')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    lg.setLogLevel(args.log)
    if args.log == 'debug':
        ipmininet.DEBUG_FLAG = True
    kwargs = {}
    for arg in args.args.strip(' \r\t\n').split(','):
        arg = arg.strip(' \r\t\n')
        if not arg:
            continue
        try:
            k, v = arg.split('=')
            kwargs[k] = v
        except ValueError:
            lg.error('Ignoring args:', arg)
    net = SR6Net(topo=TOPOS[args.topo](**kwargs), **NET_ARGS.get(args.topo, {}))
    net.start()
    SR6CLI(net)
    net.stop()
