"""This file contains a simple OSPFv3 topology with SRv6 routes"""

from ipmininet.iptopo import IPTopo

HOSTS_PER_ROUTER = 2


class DelaySRNet(IPTopo):

    def __init__(self, *args,  **kwargs):
        super(DelaySRNet, self).__init__(*args, **kwargs)

    def build(self, *args, **kwargs):
        client = self.addRouter('client', lo_addresses=["fc11::1/128"])
        server = self.addRouter('server', lo_addresses=["fc11::2/128"])
        r3 = self.addRouter('r3', lo_addresses=["fc11::3/128"])
        r4 = self.addRouter('r4', lo_addresses=["fc11::4/128"])
        r5 = self.addRouter('r5', lo_addresses=["fc11::5/128"])
        r6 = self.addRouter('r6', lo_addresses=["fc11::6/128"])
        r7 = self.addRouter('r7', lo_addresses=["fc11::7/128"])
        r8 = self.addRouter('r8', lo_addresses=["fc11::8/128"])

        self.addLink(client, r3, ip1="2042:13::1/64", ip2="2042:13::3/64")
        self.addLink(r3, r4, ip1="2042:34::3/64", ip2="2042:34::4/64", delay="20ms")
        self.addLink(r4, r5, ip1="2042:45::4/64", ip2="2042:45::5/64")
        self.addLink(r5, server, ip1="2042:52::5/64", ip2="2042:52::2/64")

        self.addLink(client, r6, ip1="2042:16::1/64", ip2="2042:16::6/64")
        self.addLink(r6, r7, ip1="2042:67::6/64", ip2="2042:67::7/64", delay="40ms")
        self.addLink(r7, r8, ip1="2042:78::7/64", ip2="2042:78::8/64")
        self.addLink(r8, server, ip1="2042:82::8/64", ip2="2042:82::2/64")

        super(DelaySRNet, self).build(*args, **kwargs)

    def addLink(self, node1, node2, ip1=None, ip2=None, params1=None, params2=None,
                delay="1ms", bw=100,  **kwargs):

        params1 = {} if params1 is None else params1
        params2 = {} if params2 is None else params2
        if ip1 is not None:
            params1.setdefault("ip", [ip1])
        if ip2 is not None:
            params2.setdefault("ip", [ip2])

        for tc_param, value in {"bw": bw, "delay": delay}.items():
            if value is not None:
                params1[tc_param] = value
                params2[tc_param] = value

        kwargs["params1"] = params1
        kwargs["params2"] = params2
        print(kwargs)
        return super(DelaySRNet, self).addLink(node1, node2, **kwargs)
