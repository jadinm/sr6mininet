"""This file contains a simple OSPFv3 topology with SRv6 routes"""

from ipmininet.iptopo import IPTopo

from sr6mininet.sr6router import SR6Config

HOSTS_PER_ROUTER = 2


class ECNSRNet(IPTopo):

    def __init__(self, red_min=1000, red_max=2000, red_avpkt=1000,
                 red_probability=0.9, red_burst=1, red_limit=1, *args,  **kwargs):
        self.red_min = red_min
        self.red_max = red_max
        self.red_avpkt = red_avpkt
        self.red_probability = red_probability
        self.red_burst = red_burst
        self.red_limit = red_limit
        super(ECNSRNet, self).__init__(*args, **kwargs)

    def build(self, *args, **kwargs):
        client = self.addRouter('client', lo_addresses=["fc11::1/128"])
        server = self.addRouter('server', lo_addresses=["fc11::2/128"])
        r3 = self.addRouter('r3', lo_addresses=["fc11::3/128"])
        r4 = self.addRouter('r4', lo_addresses=["fc11::4/128"])
        r5 = self.addRouter('r5', lo_addresses=["fc11::5/128"])
        r6 = self.addRouter('r6', lo_addresses=["fc11::6/128"])
        r7 = self.addRouter('r7', lo_addresses=["fc11::7/128"])
        r8 = self.addRouter('r8', lo_addresses=["fc11::8/128"])
        r9 = self.addRouter('r9', lo_addresses=["fc11::9/128"])
        r10 = self.addRouter('r10', lo_addresses=["fc11::a/128"])
        r11 = self.addRouter('r11', lo_addresses=["fc11::b/128"])

        self.addLink(client, r3, ip1="2042:13::1/64", ip2="2042:13::3/64")
        self.addLink(r3, r4, ip1="2042:34::3/64", ip2="2042:34::4/64")
        self.addLink(r4, r5, ip1="2042:45::4/64", ip2="2042:45::5/64")
        self.addLink(r5, server, ip1="2042:52::5/64", ip2="2042:52::2/64")

        self.addLink(client, r6, ip1="2042:16::1/64", ip2="2042:16::6/64", igp_metric=2)
        self.addLink(r6, r7, ip1="2042:67::6/64", ip2="2042:67::7/64")
        self.addLink(r7, r8, ip1="2042:78::7/64", ip2="2042:78::8/64")
        self.addLink(r8, server, ip1="2042:82::8/64", ip2="2042:82::2/64")

        self.addLink(client, r9, ip1="2042:19::1/64", ip2="2042:19::9/64", igp_metric=2)
        self.addLink(r9, r10, ip1="2042:9a::9/64", ip2="2042:9a::a/64")
        self.addLink(r10, r11, ip1="2042:ab::a/64", ip2="2042:ab::b/64")
        self.addLink(r11, server, ip1="2042:b2::b/64", ip2="2042:b2::2/64")

        super(ECNSRNet, self).build(*args, **kwargs)

    def addRouter(self, *args, **kwargs):
        return super(ECNSRNet, self).addRouter(*args, config=ECNConfig, **kwargs)

    def addLink(self, node1, node2, ip1=None, ip2=None, params1=None, params2=None,
                delay="1ms", bw=100,  **kwargs):

        params1 = {} if params1 is None else params1
        params2 = {} if params2 is None else params2
        if ip1 is not None:
            params1.setdefault("ip", [ip1])
        if ip2 is not None:
            params2.setdefault("ip", [ip2])

        params1.setdefault("enable_ecn", True)
        params2.setdefault("enable_ecn", True)
        for tc_param, value in {"bw": bw, "delay": delay,
                                "red_min": self.red_min, "red_max": self.red_max,
                                "red_burst": self.red_burst, "red_avpkt": self.red_avpkt,
                                "red_probability": self.red_probability,
                                "red_limit": self.red_limit * bw * 10**6}.items():
            if value is not None:
                params1[tc_param] = value
                params2[tc_param] = value

        kwargs["params1"] = params1
        kwargs["params2"] = params2
        print(kwargs)
        return super(ECNSRNet, self).addLink(node1, node2, **kwargs)


class ECNConfig(SR6Config):
    def build(self):
        self.sysctl = "net.ipv4.tcp_ecn=1"
        super(ECNConfig, self).build()
