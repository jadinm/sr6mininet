"""This file contains a simple OSPFv3 topology with SRv6 routes"""

from ipmininet.iptopo import IPTopo

HOSTS_PER_ROUTER = 2


class LossySRNet(IPTopo):

    def __init__(self, loss="5", *args, **kwargs):
        self.loss = int(loss)
        super(LossySRNet, self).__init__(*args, **kwargs)

    def build(self, *args, **kwargs):
        """
                                 +----+ loss = 5%
                            +----+ R2 +-----+
                            |    +----+     |
                            |               |
                         +--+-+          +--+-+
                         | R1 |          | R4 |
                         +--+-+          +-+--+
                            |              |
                            | 2  +----+    |
                            +----+ R3 +----+
                                 +----+

        The upper path between R1 and R4 is lossy while the other one is good.
        The lower path is not preferred by IGP but can be used with SRv6
        No rule are installed, this network is intended for manual testing
        """
        r1, r2, r3, r4 = self.addRouter('r1', lo_addresses=["fc11::1/128"]),\
                         self.addRouter('r2', lo_addresses=["fc11::2/128"]),\
                         self.addRouter('r3', lo_addresses=["fc11::3/128"]),\
                         self.addRouter('r4', lo_addresses=["fc11::4/128"])
        self.addLink(r1, r2, delay="1ms", bw=100,
                     ip1="2042:12::1/64", ip2="2042:12::2/64")
        self.addLink(r1, r3, delay="1ms", bw=100, igp_metric=2,
                     ip1="2042:13::1/64", ip2="2042:13::3/64")
        self.addLink(r2, r4, delay="1ms", bw=100, loss=self.loss,
                     ip1="2042:24::2/64", ip2="2042:24::4/64")
        self.addLink(r3, r4, delay="1ms", bw=100,
                     ip1="2042:34::3/64", ip2="2042:34::4/64")

        super(LossySRNet, self).build(*args, **kwargs)

    def addLink(self, node1, node2, ip1=None, ip2=None, params1=None, params2=None,
                bw=None, delay=None, loss=None,  **kwargs):

        params1 = {} if params1 is None else params1
        params2 = {} if params2 is None else params2
        if ip1 is not None:
            params1.setdefault("ip", [ip1])
        if ip2 is not None:
            params2.setdefault("ip", [ip2])

        for tc_param, value in {"bw": bw, "delay": delay, "loss": loss}.items():
            if value is not None:
                params1[tc_param] = value
                params2[tc_param] = value

        kwargs["params1"] = params1
        kwargs["params2"] = params2
        print(kwargs)
        return super(LossySRNet, self).addLink(node1, node2, **kwargs)
