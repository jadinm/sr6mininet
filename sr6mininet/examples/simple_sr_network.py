"""This file contains a simple OSPFv3 topology with SRv6 routes"""

from ipmininet.iptopo import IPTopo
from sr6mininet.sr6router import SRv6Encap, SRv6Route

HOSTS_PER_ROUTER = 2


class SimpleSRNet(IPTopo):
    def build(self, *args, **kwargs):
        """
                                 +----+
                            +----+ R1 +-----+
                            |    +----+     |
                            |               |
                         +--+-+          +--+-+
        +----+           | R2 +----------+ R3 |           +----+
        | R5 +-----------++---+          +-+--+-----------+ R6 |
        +--+-+            |                |              +--+-+
           |2             |2               |                 |
           |              |                |                 |
        +--+-+            |                |              +--+-+
        | R4 +------------+                +--------------+ R7 |
        +----+                                            +----+

        Two hosts are attached to each router, named as hXY where x is the
        host number attached to that router, and Y the router name.

        IPv6 Segment Routing rules:
            R2: If destination is an host of R5, go through R4
            R3: If destination is an host of R6, go through R7
        """
        r1, r2 = self.addRouter('r1'), self.addRouter('r2')
        r3 = self.addRouter('r3')
        self.addLink(r1, r2)
        self.addLink(r1, r3)
        self.addLink(r3, r2)
        for r in (r1, r2, r3):
            for i in xrange(HOSTS_PER_ROUTER):
                self.addLink(r, self.addHost('h%s%s' % (i, r)))

        r4, r5 = self.addRouter('r4'), self.addRouter('r5')
        self.addLink(r2, r5)
        self.addLink(r2, r4, igp_metric=2)
        self.addLink(r4, r5, igp_metric=2)
        for r in (r4, r5):
            for i in xrange(HOSTS_PER_ROUTER):
                self.addLink(r, self.addHost('h%s%s' % (i, r)))

        r6, r7 = self.addRouter('r6'), self.addRouter('r7')
        self.addLink(r3, r6)
        self.addLink(r3, r7)
        self.addLink(r6, r7)
        for r in (r6, r7):
            for i in xrange(HOSTS_PER_ROUTER):
                self.addLink(r, self.addHost('h%s%s' % (i, r)))

        self.addOverlay(SRv6Encap(router=r2, destinations=['h%s%s' % (i, r5) for i in xrange(HOSTS_PER_ROUTER)],
                                  mode=SRv6Route.ENCAP, path=[r4]))
        self.addOverlay(SRv6Encap(router=r3, destinations=['h%s%s' % (i, r6) for i in xrange(HOSTS_PER_ROUTER)],
                                  mode=SRv6Route.INLINE, path=[r7]))

        super(SimpleSRNet, self).build(*args, **kwargs)
