from ipmininet.host import IPHost
from ipmininet.utils import realIntfList


class SR6Host(IPHost):

    def enable_srv6(self):
        """
        Enables SRv6 on all interfaces
        """
        self._set_sysctl("net.ipv6.conf.all.seg6_enabled", "1")
        for intf in realIntfList(self):
            self._set_sysctl("net.ipv6.conf.%s.seg6_enabled" % intf.name, "1")
