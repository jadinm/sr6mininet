from ipmininet.link import TCIntf
from mininet.link import Intf
from mininet.log import lg


class SR6TCIntf(TCIntf):

    def __init__(self, *args, **kwargs):
        super(SR6TCIntf, self).__init__(*args, **kwargs)
        self.delay = kwargs.get("delay", "0ms")
        self.bw = kwargs.get("bw", 0)

    def config( self, bw=None, delay=None, jitter=None, loss=None,
                gro=False, txo=True, rxo=True,
                speedup=0, use_hfsc=False, use_tbf=False,
                latency_ms=None, enable_ecn=False, enable_red=False,
                max_queue_size=None, red_limit=1000000, red_avpkt=1500,
                red_probability=1, red_min=30000, red_max=35000, red_burst=20,
                **params ):
        """Configure the port and set its properties.
           bw: bandwidth in b/s (e.g. '10m')
           delay: transmit delay (e.g. '1ms' )
           jitter: jitter (e.g. '1ms')
           loss: loss (e.g. '1%' )
           gro: enable GRO (False)
           txo: enable transmit checksum offload (True)
           rxo: enable receive checksum offload (True)
           speedup: experimental switch-side bw option
           use_hfsc: use HFSC scheduling
           use_tbf: use TBF scheduling
           latency_ms: TBF latency parameter
           enable_ecn: enable ECN (False)
           enable_red: enable RED (False)
           max_queue_size: queue limit parameter for netem"""

        # Support old names for parameters
        gro = not params.pop( 'disable_gro', not gro )

        result = Intf.config( self, **params)

        def on( isOn ):
            "Helper method: bool -> 'on'/'off'"
            return 'on' if isOn else 'off'

        # Set offload parameters with ethool
        self.cmd( 'ethtool -K', self,
                  'gro', on( gro ),
                  'tx', on( txo ),
                  'rx', on( rxo ) )

        # Optimization: return if nothing else to configure
        # Question: what happens if we want to reset things?
        if ( bw is None and not delay and not loss
             and max_queue_size is None ):
            return

        # Clear existing configuration
        tcoutput = self.tc( '%s qdisc show dev %s' )
        if "priomap" not in tcoutput and "noqueue" not in tcoutput:
            cmds = [ '%s qdisc del dev %s root' ]
        else:
            cmds = []

        # Bandwidth limits via various methods
        bwcmds, parent = self.bwCmds( bw=bw, speedup=speedup,
                                      use_hfsc=use_hfsc, use_tbf=use_tbf,
                                      latency_ms=latency_ms,
                                      enable_ecn=enable_ecn,
                                      enable_red=enable_red,
                                      red_limit=red_limit,
                                      red_avpkt=red_avpkt,
                                      red_probability=red_probability,
                                      red_min=red_min,
                                      red_max=red_max,
                                      red_burst=red_burst )
        cmds += bwcmds

        # Delay/jitter/loss/max_queue_size using netem
        delaycmds, parent = self.delayCmds( delay=delay, jitter=jitter,
                                            loss=loss,
                                            max_queue_size=max_queue_size,
                                            parent=parent )
        cmds += delaycmds

        # Ugly but functional: display configuration info
        stuff = ( ( [ '%.2fMbit' % bw ] if bw is not None else [] ) +
                  ( [ '%s delay' % delay ] if delay is not None else [] ) +
                  ( [ '%s jitter' % jitter ] if jitter is not None else [] ) +
                  ( ['%d%% loss' % loss ] if loss is not None else [] ) +
                  ( [ 'ECN' ] if enable_ecn else [ 'RED' ]
                    if enable_red else [] ) )
        lg.info( '(' + ' '.join( stuff ) + ') ' )

        # Execute all the commands in our node
        lg.debug("at map stage w/cmds: %s\n" % cmds)
        tcoutputs = [ self.tc(cmd) for cmd in cmds ]
        for output in tcoutputs:
            if output != '':
                lg.error( "*** Error: %s" % output )
        lg.debug( "cmds:", cmds, '\n' )
        lg.debug( "outputs:", tcoutputs, '\n' )
        result[ 'tcoutputs'] = tcoutputs
        result[ 'parent' ] = parent

        return result

    def delayCmds(self, parent, delay=None, jitter=None,
                  loss=None, max_queue_size=None):
        "Internal method: return tc commands for delay and loss"
        cmds = []
        if jitter and jitter < 0:
            lg.error('Negative jitter', jitter, '\n')
        elif loss and (loss < 0 or loss > 100):
            lg.error('Bad loss percentage', loss, '%%\n')
        else:
            # Delay/jitter/loss/max queue size
            netemargs = '%s%s%s%s' % (
                'delay %s ' % delay if delay is not None else '',
                '%s ' % jitter if jitter is not None else '',
                'loss %0.4f ' % loss if loss is not None and loss > 0 else '',  # The fix
                'limit %d' % max_queue_size if max_queue_size is not None
                else '')
            if netemargs:
                cmds = ['%s qdisc add dev %s ' + parent +
                        ' handle 10: netem ' +
                        netemargs]
                parent = ' parent 10:1 '
        return cmds, parent

    def bwCmds(self, bw=None, speedup=0, use_hfsc=False, use_tbf=False,
               latency_ms=None, enable_ecn=False, enable_red=False,
               red_limit=1000000, red_avpkt=1500, red_probability=1,
               red_min=30000, red_max=35000, red_burst=20):
        "Return tc commands to set bandwidth"

        cmds, parent = [], ' root '

        if bw and (bw < 0 or bw > self.bwParamMax):
            lg.error('Bandwidth limit', bw, 'is outside supported range 0..%d'
                     % self.bwParamMax, '- ignoring\n')
        elif bw is not None:
            # BL: this seems a bit brittle...
            if (speedup > 0 and self.node.name[0:1] == 's'):
                bw = speedup

            # This may not be correct - we should look more closely
            # at the semantics of burst (and cburst) to make sure we
            # are specifying the correct sizes. For now I have used
            # the same settings we had in the mininet-hifi code.
            if use_hfsc:
                cmds += ['%s qdisc add dev %s root handle 5:0 hfsc default 1',
                         '%s class add dev %s parent 5:0 classid 5:1 hfsc sc '
                         + 'rate %fMbit ul rate %fMbit' % (bw, bw)]
            elif use_tbf:
                if latency_ms is None:
                    latency_ms = 15 * 8 / bw
                cmds += ['%s qdisc add dev %s root handle 5: tbf ' +
                         'rate %fMbit burst 15000 latency %fms' %
                         (bw, latency_ms)]
            else:
                cmds += ['%s qdisc add dev %s root handle 5:0 htb default 1',
                         '%s class add dev %s parent 5:0 classid 5:1 htb ' +
                         'rate %fMbit burst 15k' % bw]
            parent = ' parent 5:1 '

            # ECN or RED
            if enable_ecn or enable_red:
                cmds += ['%s qdisc add dev %s' + parent +
                         'handle 6: red limit {red_limit} '
                         'min {red_min} max {red_max} avpkt {red_avpkt} '
                         'burst {red_burst} '
                         'bandwidth {bw}mbit probability 1 {ecn}'.format(
                             bw=bw, red_limit=red_limit, red_min=red_min,
                             red_max=red_max, red_avpkt=red_avpkt,
                             red_burst=red_burst, ecn="ecn" if enable_ecn else "")]
                parent = ' parent 6: '
        return cmds, parent

    def __lt__(self, other):
        return self.name < other.name

    def __gt__(self, other):
        return self.name > other.name
