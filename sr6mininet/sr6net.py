import heapq
import time

from ipaddress import ip_network, ip_address
from ipmininet.ipnet import IPNet
from ipmininet.utils import realIntfList, L3Router
from mininet.log import lg

from .sr6host import SR6Host
from .sr6link import SR6TCIntf
from .sr6router import SR6Config, SR6Router


class SR6Net(IPNet):
    """SR6-aware Mininet"""

    def __init__(self,
                 router=SR6Router,
                 intf=SR6TCIntf,
                 config=SR6Config,
                 host=SR6Host,
                 ip6Base=u'fc00:42::/32',
                 ip6loBase=u'fc00:2::/32',
                 try_route_timeout=4,
                 static_routing=False,
                 *args, **kwargs):
        self.static_routing = static_routing
        self.ip6loBase = ip6loBase
        self.try_route_timeout = try_route_timeout
        super(SR6Net, self).__init__(*args, router=router, intf=intf, use_v4=False, use_v6=True, config=config,
                                     host=host, ip6Base=ip6Base, **kwargs)

    def addRouter(self, name, cls=None, **params):
        params["static_routing"] = self.static_routing
        super(SR6Net, self).addRouter(name, cls, **params)

    def addLink(self, *args, **params):
        defaults = {"intf": self.intf}
        defaults.update(params)
        super(SR6Net, self).addLink(*args, **defaults)

    def pre_router_start(self):
        if self.static_routing:
            lg.output("*** Inserting static routes\n")
            for r in self.routers:
                lans, loopbacks = find_closest_paths(r)
                for lan, routes in lans.items():
                    self._add_static_routes_to_itf(r, lan, routes)
                for node, routes in loopbacks.items():
                    self._add_static_routes_to_itf(r, node, routes)

    def start(self):
        # Controller nodes must be started first (because of ovsdb daemon)
        super(SR6Net, self).start()

        # Enable SRv6 on all hosts
        for host in self.hosts:
            host.enable_srv6()

    def _try_add_route(self, node, cmd):
        """Try for some time to insert the route
           If addition is tried directly, the operation is likely to fail."""
        out = node.cmd(cmd)
        step = 10
        for i in range(0, self.try_route_timeout * 1000, step):
            if len(out) == 0:
                return out
            time.sleep(step / 1000.)
            out = node.cmd(cmd)
        return out

    def _add_static_routes_to_itf(self, r, dest, routes):
        """Add static the "routes" between "r" and "dest_itf" as an IGP protocol would do."""
        dest_itf = routes[0][1]  # dest_itfs in routes are all on the same LAN and thus have the same prefixes
        for ip6 in dest_itf.ip6s(exclude_lls=True, exclude_lbs=True):
            dest_prefix = ip6.network.with_prefixlen

            if len(routes) == 1:
                cost, _, direct_peer_itf = routes[0]
                if ip_address(direct_peer_itf.ip6) in ip_network(dest_prefix):
                    continue  # Already a route for this prefix
                cmd = ["ip", "-6", "route", "add", dest_prefix,
                       "via", direct_peer_itf.ip6, "metric", str(cost)]
                out = self._try_add_route(r, cmd)
                if len(out) > 0:
                    lg.error("Route from %s to %s<%s>: " % (r.name, dest, dest_itf.name) + " ".join(cmd) + "\n")
                    lg.error(out)
            elif len(routes) > 0:
                cmd = ["ip", "-6", "route", "add", dest_prefix, "metric", str(routes[0][0])]
                for cost, _, direct_peer_itf in routes:
                    if ip_address(direct_peer_itf.ip6) in ip_network(dest_prefix):
                        continue  # Already a route for this prefix
                    cmd.extend(["nexthop", "via", direct_peer_itf.ip6, "weight", "1"])
                out = self._try_add_route(r, cmd)
                if len(out) > 0:
                    lg.error("Route from %s to %s<%s>: " % (r.name, dest, dest_itf.name) + " ".join(cmd) + "\n")
                    lg.error(out)


def cost_intf(intf):
    return intf.igp_metric


def find_closest_paths(base):
    """Find the dict {router: [(path_cost, direct_peer_itf)]} for a minimum path_cost for each router
       and the dict {(host, host_itf): [(path_cost, direct_peer_itf)]} for a minimum path_cost for each interface of an host.
       It takes into account ECMP paths."""
    visited = set()
    to_visit = [(cost_intf(intf), intf, None) for intf in realIntfList(base)]
    heapq.heapify(to_visit)
    lans = {}
    loopbacks = {}

    # Explore all interfaces in base ASN recursively
    while to_visit:
        cost, intf, direct_next_itf = heapq.heappop(to_visit)
        visited.add(intf.name)
        for peer_intf in intf.broadcast_domain:
            if peer_intf.node.name == base.name or peer_intf.name == intf.name:
                continue

            # Update path to LANs
            destination = frozenset(intf.broadcast_domain.interfaces)

            min_paths = lans.get(destination, None)
            if not min_paths or min_paths[0][0] > cost:
                lans[destination] = [(cost, peer_intf, direct_next_itf if direct_next_itf else peer_intf)]
            elif min_paths[0][0] == cost:  # ECMP
                found = False
                for path in min_paths:
                    if path[2].name == direct_next_itf.name:
                        found = True
                        break
                if not found:
                    min_paths.append((cost, peer_intf, direct_next_itf if direct_next_itf else peer_intf))

            # Update path to loopbacks
            if L3Router.is_l3router_intf(peer_intf):
                peer = peer_intf.node.name
                lo_itf = peer_intf.node.intf("lo")
                lo_min_paths = loopbacks.get(peer, None)

                if not lo_min_paths or lo_min_paths[0][0] > cost:
                    loopbacks[peer] = [(cost, lo_itf, direct_next_itf if direct_next_itf else peer_intf)]
                elif lo_min_paths[0][0] == cost:  # ECMP
                    found = False
                    for path in lo_min_paths:
                        if path[2].name == direct_next_itf.name:
                            found = True
                            break
                    if not found:
                        lo_min_paths.append((cost, lo_itf, direct_next_itf if direct_next_itf else peer_intf))

            # Hosts don't forward packets
            if L3Router.is_l3router_intf(peer_intf):
                for x in realIntfList(peer_intf.node):
                    if x.name != peer_intf.name and x.name not in visited:
                        heapq.heappush(to_visit, (cost + cost_intf(x), x, direct_next_itf if direct_next_itf else peer_intf))

    # Add routes to loopbacks, i.e., the route with the shortest path to a lan of the node

    return lans, loopbacks
