import mininet.clean
import sys
from mininet.log import lg

from ipmininet.iptopo import Overlay
from ipmininet.router import Router
from ipmininet.router.config import RouterConfig
from ipmininet.router.config.ospf6 import OSPF6RedistributedRoute
from ipmininet.utils import realIntfList


class SR6Config(RouterConfig):

    def __init__(self, node, additional_daemons=(), *args, **kwargs):
        """A simple router made of at least an OSPF daemon

        :param additional_daemons: Other daemons that should be used"""
        # Importing here to avoid circular import
        from ipmininet.router.config.ospf import OSPF
        from ipmininet.router.config.ospf6 import OSPF6
        # We don't want any zebra-specific settings, so we rely on the OSPF/OSPF6
        # DEPENDS list for that daemon to run it with default settings
        # We also don't want specific settings beside the defaults, so we don't
        # provide an instance but the class instead
        d = []
        if node.use_v4 and not node.static_routing:
            d.append(OSPF)
        if node.use_v6 and not node.static_routing:
            d.append((OSPF6, {'redistribute': [OSPF6RedistributedRoute("connected")]}))
        d.extend(additional_daemons)
        super(SR6Config, self).__init__(node, daemons=d,
                                        *args, **kwargs)

    def build(self):
        self.sysctl = "net.ipv6.conf.all.seg6_enabled=1"
        for intf in realIntfList(self._node):
            self.sysctl = "net.ipv6.conf.%s.seg6_enabled=1" % intf.name
        super(SR6Config, self).build()


class SRv6Route:
    """A class representing a SRv6 route"""
    ENCAP = "encap"
    INLINE = "inline"

    def __init__(self, destination, path, mode=ENCAP, metric=1):
        self.destination = destination
        self.path = path
        self.mode = mode
        self.metric = metric

    def dest_prefixes(self, node):
        destination = self.find_node(node, self.destination)
        prefixes = []
        if isinstance(destination, SR6Router):  # Get the SID
            for ip6 in destination.intf('lo').ip6s(exclude_lls=True, exclude_lbs=True):
                prefixes.append(ip6.network.with_prefixlen)
        else:  # Get the interface addresses for hosts
            for itf in realIntfList(destination):
                for ip6 in itf.ip6s(exclude_lls=True, exclude_lbs=True):
                    prefixes.append(ip6.network.with_prefixlen)
        return prefixes

    def segments(self, node):
        s = []
        for node_name in self.path:
            node = self.find_node(node, node_name)
            for ip6 in node.intf('lo').ip6s(exclude_lls=True, exclude_lbs=True):
                s.append(ip6.ip.compressed)
        return s

    @staticmethod
    def find_node(base, node_name):
        if base.name == node_name:
            return base
        visited = set()
        to_visit = [intf for intf in realIntfList(base)]

        while to_visit:
            intf = to_visit[0]
            to_visit = to_visit[1:]
            if intf in visited:
                continue
            visited.add(intf)
            for peer_intf in intf.broadcast_domain:
                if peer_intf.node.name == node_name:
                    return peer_intf.node
                else:
                    for x in realIntfList(peer_intf.node):
                        to_visit.append(x)
        return None

    def install(self, node):
        itfs = realIntfList(node)
        if len(itfs) == 0:
            lg.error(node.name, 'Cannot install SRv6Route without a real interface (no \'lo\')\n')
            return -1
        for prefix in self.dest_prefixes(node):
            cmd = "ip route add %s encap seg6 mode %s segs %s metric %s dev %s" \
                  % (prefix, self.mode, ",".join(self.segments(node)), self.metric, itfs[0].name)
            out, err, code = node.pexec(cmd)
            lg.info("Installing route on router %s: '%s'" % (node.name, cmd))
            if code:
                lg.error(node.name, 'Cannot install SRv6Route [rcode:', str(code),
                         ']\nstdout:', str(out), '\nstderr:', str(err))
                return code
        return 0


class SRv6Encap(Overlay):

    def __init__(self, router, destinations, path, mode=SRv6Route.ENCAP, metric=1):
        super(SRv6Encap, self).__init__(nodes=[router])
        self.routes = [SRv6Route(dest, path, mode, metric) for dest in destinations]

    def apply(self, topo):
        # Other routes might be added by another SRv6Encap
        for n in self.nodes:
            routes = topo.nodeInfo(n).get("srv6_routes", [])
            routes.extend(self.routes)
            topo.nodeInfo(n).update({"srv6_routes": routes})
        super(SRv6Encap, self).apply(topo)


class SR6Router(Router):

    def __init__(self, name, config=SR6Config, cwd="/tmp", static_routing=False,
                 srv6_routes=None, *args, **kwargs):
        # Variables defined before to be accessible for config daemons
        self.static_routing = static_routing
        self.srv6_routes = srv6_routes
        super(SR6Router, self).__init__(name, config=config, cwd=cwd, *args, **kwargs)

    def start(self):
        super(SR6Router, self).start()

        # Set SRv6 source address for encapsulation
        for ip6 in self.intf('lo').ip6s(exclude_lls=True, exclude_lbs=True):
            cmd = ["ip", "sr", "tunsrc", "set", ip6.ip.compressed]
            out, err, code = self.pexec(cmd)
            if code:
                lg.error(self.name, 'Cannot set SRv6 source address [rcode:', str(code),
                         ']\nstdout:', str(out), '\nstderr:', str(err))
            break

        # Install SRv6 Routes
        if self.srv6_routes is not None:
            for route in self.srv6_routes:
                code = route.install(self)
                if code:
                    lg.error('SRv6 install failed, aborting!')
                    mininet.clean.cleanup()
                    sys.exit(1)
